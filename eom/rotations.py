# 3D rotation matrices
# COPYRIGHT (c) 2019 Pinetree LLC & Aptus Engineering, Inc.

import sympy as np

# Rotation by a radians about the x-axis
def rotx(a):
	return np.Matrix([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])

# Rotation by a radians about the y-axis
def roty(a):
	return np.Matrix([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])

# Rotation by a radians about the z-axis
def rotz(a):
	return np.Matrix([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])