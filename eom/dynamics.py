# Dynamic modeling helper functions
# COPYRIGHT (c) 2019 Pinetree LLC & Aptus Engineering, Inc.

import sympy as np

# Kinetic Energy function
def KineticEnergy(mB, IIrBdot, ITBdot, BBrCM, BBJ):

	# Pseudo Inertia tensor
	BBJhat = 0.5 * np.trace(BBJ) * np.eye(3) - BBJ

	K = 0

	# Translational kinetic energy
	K += 0.5 * mB * (np.transpose(IIrBdot) * IIrBdot)[0,0]

	# Rotational kinetic energy
	K += 0.5 * np.trace(ITBdot * BBJhat * np.transpose(ITBdot))

	# Coupling kinetic energy
	K += mB * (np.transpose(IIrBdot) * ITBdot * BBrCM)[0,0]

	return K


# Potential Energy function
def PotentialEnergy(mB, IIrB, ITB, BBrCM, g):

	return mB * (np.transpose(g) * (IIrB + ITB * BBrCM))[0,0]


# Differentiate using chain rule
def ChainDiff(y, x, xd):
	
	ydot = np.zeros(y.shape[0], y.shape[1])

	for n in range(x.shape[0]):
		ydot += np.diff(y, x[n]) * xd[n]
	
	return ydot