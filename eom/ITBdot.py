# Dynamix Model Definition for a rigid body
# Authors: Narendran Muraleedharan <narendran.m@aptus.aero>
#
# COPYRIGHT (c) 2019 Pinetree LLC & Aptus Engineering, Inc.

# Dependencies: sympy
import sympy as np

from rotations import *
from dynamics import *
from sympy.utilities.codegen import codegen

phi = np.symbols('phi')
theta = np.symbols('theta')
psi = np.symbols('psi')

phid = np.symbols('phid')
thetad = np.symbols('thetad')
psid = np.symbols('psid')

phidd = np.symbols('phidd')
thetadd = np.symbols('thetadd')
psidd = np.symbols('psidd')

# State vector and state vector rates
X = np.Matrix([phi, theta, psi, phid, thetad, psid])
Xd = np.Matrix([phid, thetad, psid, phidd, thetadd, psidd])

ITb = rotz(psi) * roty(theta) * rotx(phi)
ITbd = ChainDiff(ITb, X, Xd)


[(_, ITbd_code), (_, _)] = codegen([("ITbd", ITbd)], "C99", header=False, empty=True)
print(ITbd_code)