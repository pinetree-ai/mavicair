# Dynamix Model Definition for a rigid body
# Authors: Narendran Muraleedharan <narendran.m@aptus.aero>
#
# COPYRIGHT (c) 2019 Pinetree LLC & Aptus Engineering, Inc.

# Dependencies: sympy
import sympy as np

from dynamics import *
from rotations import *
from sympy.utilities.codegen import codegen

# Constants
# Acceleration due to gravity
g = np.transpose(np.Matrix([[0, 0, np.symbols('gz')]]))

## Setup generalized coordinates, velocities and accelerations as symbolic variables
# Generalized coordinates
x = np.symbols('x')
y = np.symbols('y')
z = np.symbols('z')
phi = np.symbols('phi')
theta = np.symbols('theta')
psi = np.symbols('psi')
m1 = np.symbols('m1')
m2 = np.symbols('m2')
m3 = np.symbols('m3')
m4 = np.symbols('m4')
gamma = np.Matrix([x, y, z, phi, theta, psi, m1, m2, m3, m4])

# Generalized velocities
xd = np.symbols('xd')
yd = np.symbols('yd')
zd = np.symbols('zd')
phid = np.symbols('phid')
thetad = np.symbols('thetad')
psid = np.symbols('psid')
m1d = np.symbols('m1d')
m2d = np.symbols('m2d')
m3d = np.symbols('m3d')
m4d = np.symbols('m4d')
gammad = np.Matrix([xd, yd, zd, phid, thetad, psid, m1d, m2d, m3d, m4d])

# Generalized accelerations
xdd = np.symbols('xdd')
ydd = np.symbols('ydd')
zdd = np.symbols('zdd')
phidd = np.symbols('phidd')
thetadd = np.symbols('thetadd')
psidd = np.symbols('psidd')
m1dd = np.symbols('m1dd')
m2dd = np.symbols('m2dd')
m3dd = np.symbols('m3dd')
m4dd = np.symbols('m4dd')
gammadd = np.Matrix([xdd, ydd, zdd, phidd, thetadd, psidd, m1dd, m2dd, m3dd, m4dd])

# State vector and state vector rates
X = np.Matrix([x, y, z, phi, theta, psi, m1, m2, m3, m4, xd, yd, zd, phid, thetad, psid, m1d, m2d, m3d, m4d])
Xd = np.Matrix([xd, yd, zd, phid, thetad, psid, m1d, m2d, m3d, m4d, xdd, ydd, zdd, phidd, thetadd, psidd, m1dd, m2dd, m3dd, m4dd])


## Setup frames
# Body
# Mass
m = np.symbols('m')

# Center of mass offsets
cm = np.zeros(3,1)

# Inertia tensor
J = np.zeros(3,3)
J[0,0] = np.symbols('Jxx')
J[1,1] = np.symbols('Jyy')
J[2,2] = np.symbols('Jzz')

# Motors
motor_m = np.symbols('motor_m')

motor_cm = np.zeros(3,1)

# Inertia tensor
motor_J = np.zeros(3,3)
motor_J[0,0] = np.symbols('motor_Jxx')
motor_J[1,1] = np.symbols('motor_Jyy')
motor_J[2,2] = np.symbols('motor_Jzz')

motor_rx = np.symbols('motor_rx')
motor_ry = np.symbols('motor_ry')
motor_rz = np.symbols('motor_rz')

## Compute system Kinetic and Potential energies
K = 0
U = 0

# Body
IIrb = np.Matrix([x, y, z])
IIrbd = ChainDiff(IIrb, X, Xd)
ITb = rotz(psi) * roty(theta) * rotx(phi)
ITbd = ChainDiff(ITb, X, Xd)

K += KineticEnergy(m, IIrbd, ITbd, cm, J)
U += PotentialEnergy(m, IIrb, ITb, cm, g)

# Motors
IIr1 = IIrb + ITb * np.Matrix([motor_rx, motor_ry, motor_rz])
IIr1d = ChainDiff(IIr1, X, Xd)
IT1 = ITb * rotz(m1)
IT1d = ChainDiff(IT1, X, Xd)

K += KineticEnergy(motor_m, IIr1d, IT1d, motor_cm, motor_J)
U += PotentialEnergy(motor_m, IIr1, IT1, motor_cm, g)

IIr2 = IIrb + ITb * np.Matrix([motor_rx, -motor_ry, motor_rz])
IIr2d = ChainDiff(IIr2, X, Xd)
IT2 = ITb * rotz(m2)
IT2d = ChainDiff(IT2, X, Xd)

K += KineticEnergy(motor_m, IIr2d, IT2d, motor_cm, motor_J)
U += PotentialEnergy(motor_m, IIr2, IT2, motor_cm, g)

IIr3 = IIrb + ITb * np.Matrix([-motor_rx, motor_ry, motor_rz])
IIr3d = ChainDiff(IIr3, X, Xd)
IT3 = ITb * rotz(m3)
IT3d = ChainDiff(IT3, X, Xd)

K += KineticEnergy(motor_m, IIr3d, IT3d, motor_cm, motor_J)
U += PotentialEnergy(motor_m, IIr3, IT3, motor_cm, g)

IIr4 = IIrb + ITb * np.Matrix([-motor_rx, -motor_ry, motor_rz])
IIr4d = ChainDiff(IIr4, X, Xd)
IT4 = ITb * rotz(m4)
IT4d = ChainDiff(IT4, X, Xd)

K += KineticEnergy(motor_m, IIr4d, IT4d, motor_cm, motor_J)
U += PotentialEnergy(motor_m, IIr4, IT4, motor_cm, g)

print("Started H")

## Compute H, D and G
# H: System mass matrix
H = np.transpose(np.Matrix([[K]]).jacobian(gammad)).jacobian(gammad)

print("Started D")

# D: Vector of Coriolis and centripetal forces
D = np.transpose(np.Matrix([[K]]).jacobian(gammad)).jacobian(gamma) * gammad - np.transpose(np.Matrix([[K]]).jacobian(gamma))

print("Started G")

# G: Vector of Gravitational forces
G = np.transpose(np.Matrix([[U]]).jacobian(gamma))


## Export hard-coded dynamics code
[(_, H_code), (_, _)] = codegen([("H", H)], "C99", header=False, empty=True)
print(H_code)

[(_, D_code), (_, _)] = codegen([("D", D)], "C99", header=False, empty=True)
print(D_code)

[(_, G_code), (_, _)] = codegen([("G", G)], "C99", header=False, empty=True)
print(G_code)