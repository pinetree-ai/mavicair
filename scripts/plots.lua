if init then
	attitudeplot = plot_new("Attitude", "t (s)", "attitude (rad)")
end

t = getprop("/sim/time/elapsed")

if t > 0.1 then
	plot_add(attitudeplot, "AHRS_roll", t, getprop("/control/ahrs/roll"))
	plot_add(attitudeplot, "Real_roll", t, getprop("/dynamics/gamma/phi"))
end