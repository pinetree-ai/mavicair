-- AHRS: Attitude and Heading Reference System
-- Based on the nAHRS by Narendran Muraleedharan <narendran.m@aptus.aero>

-- Initialization
if init then
    -- Load custom libraries
    package.path = "libs/lua/?.lua;" .. package.path
    mat = require("matrix")
    vec = require("vector")
    rot = require("rotation")

    -- State rotation matrix
    R = mat.zeros(3,3)

    -- Measurement rotation matrix
    M = mat.zeros(3,3)

    x = mat.zeros(3,1) -- x-axis measurement vector
    y = mat.zeros(3,1) -- y-axis measurement vector

    -- Measurement vectors
    a = mat.zeros(3,1) -- Accelerometer
    w = mat.zeros(3,1) -- Gyroscope
    m = mat.zeros(3,1) -- Magnetometer

    -- Constants
    GZ = 9.8 -- Gravitational acceleration

    -- AHRS Tuning gains
    KN_A = 0.04
    KN_H = 0.12
    KN_L = 0.01

    print("AHRS Initialized")

    t_prev = 0

    roll = 0
    pitch = 0
    yaw = 0
end

-- Time (s)
t = getprop("/sim/time/elapsed")
dt = t - t_prev
t_prev = t

-- Get sensor readings
-- Accelerometer
a[1][1] = getprop("/sensors/imu/accelerometer/ax-m_s2")
a[2][1] = getprop("/sensors/imu/accelerometer/ay-m_s2")
a[3][1] = getprop("/sensors/imu/accelerometer/az-m_s2")
-- Gyroscope
w[1][1] = getprop("/sensors/imu/gyroscope/wx-rad_s")
w[2][1] = getprop("/sensors/imu/gyroscope/wy-rad_s")
w[3][1] = getprop("/sensors/imu/gyroscope/wz-rad_s")
-- Magnetometer
m[1][1] = getprop("/sensors/imu/magnetometer/mx-uT")
m[2][1] = getprop("/sensors/imu/magnetometer/my-uT")
m[3][1] = getprop("/sensors/imu/magnetometer/mz-uT")

-- Starts AHRS 1s in to the simulation
if t > 0.1 then

    -- Constructor Y-axis vector
    -- x-axis is essentially the accelerometer vector
    y = vec.cross(a, m) -- y = a (cross) m
    x = vec.cross(y, a) -- x = y (cross) a

    -- Normalize
    x = x / vec.mag(x)
    y = y / vec.mag(y)
    z = a / vec.mag(a)

    -- Construct measurement matrix
    M[1][1] = x[1][1]
    M[1][2] = x[2][1]
    M[1][3] = x[3][1]
    M[2][1] = y[1][1]
    M[2][2] = y[2][1]
    M[2][3] = y[3][1]
    M[3][1] = a[1][1]
    M[3][2] = a[2][1]
    M[3][3] = a[3][1]

    -- Adaptive gain computation
    noise = math.abs(vec.dot(a,a) - GZ * GZ)
    kN = KN_A / noise

    if kN > KN_H then -- Saturate maximum
        kN = KN_H
    end

    if kN < KN_L then -- Saturate minimum
        kN = KN_L
    end

    -- State update
    R[1][1] = M[1][1] * kN - (R[1][1] + dt * (R[1][2] * w[3][1] - R[1][3] * w[2][1])) * (kN - 1)
    R[1][2] = M[1][2] * kN - (R[1][2] - dt * (R[1][1] * w[3][1] - R[1][3] * w[1][1])) * (kN - 1)
    R[1][3] = M[1][3] * kN - (R[1][3] + dt * (R[1][1] * w[2][1] - R[1][2] * w[1][1])) * (kN - 1)
    R[2][1] = M[2][1] * kN - (R[2][1] + dt * (R[2][2] * w[3][1] - R[2][3] * w[2][1])) * (kN - 1)
    R[2][2] = M[2][2] * kN - (R[2][2] - dt * (R[2][1] * w[3][1] - R[2][3] * w[1][1])) * (kN - 1)
    R[2][3] = M[2][3] * kN - (R[2][3] + dt * (R[2][1] * w[2][1] - R[2][2] * w[1][1])) * (kN - 1)
    R[3][1] = M[3][1] * kN - (R[3][1] + dt * (R[3][2] * w[3][1] - R[3][3] * w[2][1])) * (kN - 1)
    R[3][2] = M[3][2] * kN - (R[3][2] - dt * (R[3][1] * w[3][1] - R[3][3] * w[1][1])) * (kN - 1)
    R[3][3] = M[3][3] * kN - (R[3][3] + dt * (R[3][1] * w[2][1] - R[3][2] * w[1][1])) * (kN - 1)

    -- Re-normalize state rotation matrix
    roll = math.atan2(R[3][2], R[3][3])
    pitch = math.atan2(-R[3][1], math.sqrt(R[3][2] * R[3][2] + R[3][3] * R[3][3]))
    yaw = math.atan2(R[2][1], R[1][1])

    -- Convert back to rotation matrix to update
    R = rot.z(yaw) * rot.y(pitch) * rot.x(roll)

end

-- Write to property tree
setprop("/control/ahrs/roll", roll)
setprop("/control/ahrs/pitch", pitch)
setprop("/control/ahrs/yaw", yaw)