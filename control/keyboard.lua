-- Manual keyboard inputs to control the drone
-- Keys: Arrows: pitch/roll
--       Pg Up/Pg Dn: Altitude +/-
--       A/D: Yaw left/right

-- Config
attitude_control_limit = 0.2
yawrate_control_limit = 0.6
alt_step = 0.05

-- Variables
roll_desired = 0
pitch_desired = 0
yawrate_desired = 0

alt_desired = getprop("/control/desired/altitude")
if alt_desired < 0 then
    alt_desired = 0
end

if getprop("/keyboard/state") == 1 then

    key = getprop("/keyboard/key")

    if key == 65361 then
        -- Left arrow key
        -- Roll left
        roll_desired = -attitude_control_limit
    
    elseif key == 65363 then
        -- Right arrow key
        -- Roll right
        roll_desired = attitude_control_limit
    
    elseif key == 65362 then
        -- Up arrow key
        -- Pitch forward
        pitch_desired = attitude_control_limit

    elseif key == 65364 then
        -- Down arrow key
        -- Pitch back
        pitch_desired = -attitude_control_limit
    
    elseif key == 65365 then
        -- Pg up key
        -- Altitude up
        alt_desired = alt_desired + alt_step
    
    elseif key == 65366 then
        -- Pg down key
        -- Altitude down
        alt_desired = alt_desired - alt_step
    
    elseif key == 97 then
        -- A key
        -- Yaw left
        yawrate_desired = yawrate_control_limit
    
    elseif key == 100 then
        -- D key
        -- Yaw right
        yawrate_desired = -yawrate_control_limit
    
    end
    
end

setprop("/control/desired/roll", roll_desired)
setprop("/control/desired/pitch", pitch_desired)
setprop("/control/desired/yaw-rate", yawrate_desired)
setprop("/control/desired/altitude", alt_desired)