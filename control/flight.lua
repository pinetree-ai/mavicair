if init then

    -- Initialize PID controllers
    roll_controller = pid_init(0.8,0,0.5)
    pitch_controller = pid_init(0.8,0,0.5)
    yaw_controller = pid_init(6,0,1)

    altitude_controller = pid_init(8,0,6)

    t_prev = 0

    hover_throttle = 8

end

-- Time (s)
t = getprop("/sim/time/elapsed")
dt = t - t_prev
t_prev = t

-- Run controller
if t > 0.2 then

    -- Get AHRS data
    roll = getprop("/control/ahrs/roll")
    pitch = getprop("/control/ahrs/pitch")
    -- yaw = getprop("/control/ahrs/yaw")

    -- Gyroscope data for yaw rate controller
    wz = getprop("/sensors/imu/gyroscope/wz-rad_s")

    -- Desired inputs
    roll_desired = getprop("/control/desired/roll")
    pitch_desired = getprop("/control/desired/pitch")
    yawrate_desired = getprop("/control/desired/yaw-rate")
    alt_desired = getprop("/control/desired/altitude")

    -- Run controllers
    roll_input = pid_run(roll_controller, dt, roll_desired - roll)
    pitch_input = pid_run(pitch_controller, dt, pitch_desired - pitch)
    yaw_input = pid_run(yaw_controller, dt, yawrate_desired - wz)

    if getprop("/control/mode/throttle") == "manual" or alt_desired > 0 then

        -- Altitude control
        if getprop("/control/mode/throttle") == "auto" then
            local delta_throttle = saturate(pid_run(altitude_controller, dt, alt_desired - X_z),-0.5,0.5)

            throttle = hover_throttle + delta_throttle
        else
            throttle = getprop("/control/desired/throttle")
        end

        -- Map control inputs to voltages
        -- Set motor voltages (0-12) (U_V1 ... U_V4)
        U_V1 = saturate(throttle + roll_input - pitch_input + yaw_input,0,12)
        U_V2 = saturate(throttle - roll_input - pitch_input - yaw_input,0,12)
        U_V3 = saturate(throttle + roll_input + pitch_input - yaw_input,0,12)
        U_V4 = saturate(throttle - roll_input + pitch_input + yaw_input,0,12)
    
    else

        -- Shut-off motors
        U_V1 = 0
        U_V2 = 0
        U_V3 = 0
        U_V4 = 0

    end

end