-- Manual joystick inputs to control the drone
-- Axis 0: roll
-- Axis 1: pitch
-- Axis 2: yaw

if init then
    -- Initialize joystick
    js = joystick_init(0)

    -- Disable auto-throttle
    setprop("/control/mode/throttle", "manual")
end

-- Joystick configuration for Logitech Extreme 3D Pro
roll = joystick_axis(js, 0)
pitch = joystick_axis(js, 1)
yawrate = joystick_axis(js, 2)
throttle = (1 - joystick_axis(js, 3))/2

-- Throttle deadband
if throttle < 0.05 then
    throttle = 0
end

setprop("/control/desired/roll", roll * 0.3)
setprop("/control/desired/pitch", -pitch * 0.3)
setprop("/control/desired/yaw-rate", -yawrate)
setprop("/control/desired/throttle", 12 * throttle)