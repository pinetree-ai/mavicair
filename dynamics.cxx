#include "Dynamix.hxx"

#define PI 3.14159265358979323846
#define GAMMA_LEN 10

// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: Xdot [gammad, gammadd]
// Inputs: X [gamma, gammad]
//		   U (control inputs)
//		   params (loaded parameters) - access with params["<param name>"]

RigidBody *quadcopter = NULL;

// Variables
vec Xdot(GAMMA_LEN * 2); // Return state velocities
vec q(6), qd(6), qdd(6); // for rigid body

vec F(3), M(3), T(3); // Temporary forces and moment vectors

vec motor_pos(3); // Motor vec for thrust
vec leg_pos(3), leg_vel(3), leg_offset(3); // Leg vecs for contact model

vec BEarth(3); // Earth's magnetic field

vec camera_pos(3); // Camera position offset

vec dynamics_cxx(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables)
{
	Xdot.zeros();

	// Instantiate RigidBody object
	if (quadcopter == NULL) {
		double m = params["m"];

		mat J(3,3);
		J(0,0) = params["Jxx"];
		J(1,1) = params["Jyy"];
		J(2,2) = params["Jzz"];

		vec cm(3);
		cm.zeros();

		quadcopter = new RigidBody(m, cm, J);

		quadcopter->g(2) = params["gz"]; // Gravitational acceleration

		T.zeros();

		BEarth.zeros();
		BEarth(0) = 1.0; // Test value: X points north

		camera_pos(0) = 0.08;
		camera_pos(1) = 0;
		camera_pos(2) = -0.0066;
	}

	// Copy generalized coordiates and velocities from previous state
	for (int i=0; i<6; i++) {
		q(i) = X(i);
		qd(i) = X(GAMMA_LEN+i);
	}

	for (int i=0; i<GAMMA_LEN; i++)
		Xdot(i) = X(GAMMA_LEN+i);

	// Setup RigidBoby object for computation
	quadcopter->setq(q, qd);
	// That also prepares w, ITb and ITbd

	// Motors/Propellers
	F.zeros();
	M.zeros();

	for (int m=0; m<4; m++) {

		if (m > 1) // Rear
			motor_pos(0) = -params["motor_rx"];
		else // Front
			motor_pos(0) = params["motor_rx"];

		if (m % 2) // Right
			motor_pos(1) = -params["motor_ry"];
		else // Left
			motor_pos(1) = params["motor_ry"];		

		motor_pos(2) = params["motor_rz"];

		// Apply Z-axis thrust at this location
		F(2) = params["motor_Ct"] * X(GAMMA_LEN+6+m) * X(GAMMA_LEN+6+m);
		// Apply force to body
		quadcopter->applyBodyForce(F);

		// Compute moment due to thrust
		quadcopter->applyBodyMoment(cross(motor_pos, F));

		// Compute motor reaction torque
		T(2) = params["motor_Cd"] * X(GAMMA_LEN+6+m) * abs(X(GAMMA_LEN+6+m));
		// Apply torque to body
		
		if (m == 0 || m == 3) // CW
			quadcopter->applyBodyMoment(T);
		else
			quadcopter->applyBodyMoment(-T);

		// Might as well solve the motor dynamics as well! :)
		// qdd = 1/J * (K*V/R - (b+ K*K/R) * qd - Cd * qd * qd)
		Xdot(GAMMA_LEN+6+m) = (params["motor_K"] * U["V" + to_string(m+1)] / params["motor_R"] - (params["motor_b"] + params["motor_K"] * params["motor_K"] / params["motor_R"]) * X(GAMMA_LEN+6+m) - T(2)) / params["motor_J"];
	}

	// Ground contact and friction models
	// Ground is at z=0
	F.zeros();
	M.zeros();

	double N = 0; // Normal force

	for (int leg=0; leg<4; leg++) {

		if (leg > 1) { // Rear
			leg_offset(0) = params["legR_rx"];
			leg_offset(2) = params["legR_rz"];

			if (leg % 2) leg_offset(1) = -params["legR_ry"]; // Right
			else leg_offset(1) = params["legR_ry"]; // Left		
		} else {
			leg_offset(0) = params["legF_rx"];
			leg_offset(2) = params["legF_rz"];

			if (leg % 2) leg_offset(1) = -params["legF_ry"]; // Right
			else leg_offset(1) = params["legF_ry"]; // Left
		}

		leg_pos = quadcopter->r + quadcopter->ITb * leg_offset;
		leg_vel = quadcopter->rd + quadcopter->ITbd * leg_offset;

		if (leg_pos(2) < 0) { // Apply a contact reaction force

			// Apply a z-axis force and appropriate moment to the body
			// Only z component in the inertial frame
			double N_tmp = -params["leg_k"] * leg_pos(2) - params["leg_b"] * leg_vel(2);

			F(2) = N_tmp;
			quadcopter->applyInertialForce(F);

			// Apply the moment corresponding to this force, and transform to inertial frame
			quadcopter->applyInertialMoment(cross(quadcopter->ITb * leg_offset, F));

			// Friction calculation (add to Normal force)
			N += N_tmp;
		}
	}

	if (N > 0) {
		F.zeros();

		if (norm(quadcopter->rd) > 0.0001) {
			// Moving, apply dynamic friction
			double F_dynamic_friction = params["leg_ud"] * N;

			// For x and y axes
			for (int i=0; i<2; i++)
				F(i) = -sign(quadcopter->rd(i)) * F_dynamic_friction;

		} else {
			// Stationary, apply static friction
			double F_static_friction = params["leg_us"] * N;

			// Check x and y axes
			for (int i=0; i<2; i++) {
				if (quadcopter->F(i) > F_static_friction)
					F(i) = -F_static_friction;
				else if (quadcopter->F(i) < -F_static_friction)
					F(i) = F_static_friction;
				else
					F(i) = -quadcopter->F(i);
			}
		}

		// Apply friction force
		quadcopter->applyInertialForce(F);
	}

	// Apply air drag
	F.zeros(); // Drag in the negative directions
	F(0) = -params["CdS_xy"] * quadcopter->rd(0) * abs(quadcopter->rd(0));
	F(1) = -params["CdS_xy"] * quadcopter->rd(1) * abs(quadcopter->rd(1));
	F(2) = -params["CdS_z"] * quadcopter->rd(2) * abs(quadcopter->rd(2));

	quadcopter->applyInertialForce(F);

	// Compute quadcopter body dynamics
	qdd = quadcopter->compute();

	for (int i=0; i<6; i++)
		Xdot(GAMMA_LEN+i) = qdd(i);
	
	// Link sensors to dynamic model
	// Data object to be sent to sensors
	vector<mat> data;

	// Accelerometer: data = {r, g, ITb}
	data.push_back(quadcopter->r);
	data.push_back(quadcopter->g);
	data.push_back(quadcopter->ITb);

	SensorManager::sensors()->find["/sensors/imu/accelerometer"]->update(data);

	// Gyroscope: data = {ITb}
	data.clear();
	data.push_back(quadcopter->ITb);
	SensorManager::sensors()->find["/sensors/imu/gyroscope"]->update(data);

	// Magnetometer: data = {ITb, B}
	data.clear();
	data.push_back(quadcopter->ITb);
	data.push_back(BEarth);
	SensorManager::sensors()->find["/sensors/imu/magnetometer"]->update(data);

	// Camera
	CameraManager::cameras()->find["front-camera"]->setTransform(quadcopter->r + quadcopter->ITb * camera_pos, quadcopter->ITb);

	return Xdot;
}
